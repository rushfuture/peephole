package com.jarsilio.android.waveup

import android.content.Context
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.multidex.BuildConfig
import androidx.multidex.MultiDexApplication
import com.jarsilio.android.common.extensions.defaultEmail
import com.jarsilio.android.common.logging.LongTagTree
import com.jarsilio.android.common.logging.PersistentTree
import com.jarsilio.android.waveup.receivers.ServiceTogglerReceiver
import org.acra.config.mailSender
import org.acra.config.notification
import org.acra.data.StringFormat
import org.acra.ktx.initAcra
import timber.log.Timber

@Suppress("unused")
class WaveUpApplication : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        initAcra()
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(LongTagTree(this))
        Timber.plant(PersistentTree(this))

        Timber.d("Registering ServiceToggler BroadcastReceiver for com.jarsilio.android.waveup.action.WAVEUP_ENABLE")
        val filter = IntentFilter("com.jarsilio.android.waveup.action.WAVEUP_ENABLE")

        val receiver = ServiceTogglerReceiver()
        ContextCompat.registerReceiver(applicationContext, receiver, filter, Context.RECEIVER_EXPORTED)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
    }

    private fun initAcra() {
        if (!BuildConfig.DEBUG) {
            initAcra {
                buildConfigClass = BuildConfig::class.java
                reportFormat = StringFormat.JSON
                mailSender {
                    mailTo = defaultEmail
                    reportFileName = "crash.txt"
                }
                notification {
                    title = getString(com.jarsilio.android.common.R.string.acra_notification_title)
                    text = getString(com.jarsilio.android.common.R.string.acra_notification_text)
                    channelName = getString(com.jarsilio.android.common.R.string.acra_notification_channel_name)
                    sendButtonText = getString(com.jarsilio.android.common.R.string.acra_notification_send)
                    discardButtonText = getString(android.R.string.cancel)
                    resSendButtonIcon = R.drawable.email_icon_gray
                    resDiscardButtonIcon = R.drawable.cancel_icon_gray
                }
            }
        }
    }
}
